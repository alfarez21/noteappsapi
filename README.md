## About Notes App

Note App is a restfullapi service that allows users to save their personal notes

**Url**
*  User Regiter POST   : http://127.0.0.1:8000/api/register
*  User Login   POST   : http://127.0.0.1:8000/api/login
*  List Users   GET    : http://127.0.0.1:8000/api/getallusers
*  Edit User    PUT    : http://127.0.0.1:8000/api/edituser/{user-id}
*  Detail User  GET    : http://127.0.0.1:8000/api/detailuser/{user-id}
*  Remove User  DELETE : http://127.0.0.1:8000/api/deleteuser/{user-id}
*  Add Note     POST   : http://127.0.0.1:8000/api/addnote
*  Detail Note  GET    : http://127.0.0.1:8000/api/detailnote/{note-id}
*  Edit Note    PUT    : http://127.0.0.1:8000/api/editnote/{note-id}
*  Remove Note  DELETE : http://127.0.0.1:8000/api/deletenote/{note-id}
*  My Notes     GET    : http://127.0.0.1:8000/api/mynotes


**CREATED BY MUHAMAD RIFKY**