<?php

use Illuminate\Http\Request;
// use Symfony\Component\Routing\Route;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth
Route::group(['prefix' => 'public'], function () {
   Route::group(['prefix' => 'user'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('register','Api\AuthController@register');
        Route::post('login','Api\AuthController@login');
    });
   });
});

Route::group(['prefix' => 'private', "midleware"=> "auth:api"], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('list','Api\UserController@GetAllUsers');
        Route::get('profile','Api\UserController@myProfile');
        Route::get('details/{id}','Api\UserController@detail');
        Route::put('edit/{id}','Api\UserController@edit');
        Route::delete('delete/{id}','Api\UserController@delete');
    });

    Route::group(['prefix' => 'notes'], function () {
        Route::post('create','Api\NoteController@addNote');
        Route::get('mynotes','Api\NoteController@myNotes');
        Route::get('detail/{id}','Api\NoteController@detail');
        Route::put('edit/{id}','Api\NoteController@edit');
        Route::delete('delete/{id}','Api\NoteController@delete');
    });
});
