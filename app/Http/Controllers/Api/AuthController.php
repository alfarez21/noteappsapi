<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\User;

class AuthController extends Controller
{
    // User Register
    public function register(Request $r){

        $r->validate([
            'name'=>'required|string|min:3',
            'email'=>'required|email|unique:users,email',
            'password'=>'required|min:8'
        ]);

        $user = User::create([
            'name'=> $r->name,
            'email'=> $r->email,
            'password'=> bcrypt($r->password),
            'api_token'=> Str::random(80)
        ]);
    
        return new UserResource($user);
    }

    // User Login
    public function login(Request $r){

        $r->validate([
            'email'=>'required|email',
            'password'=>'required|min:8'
        ]);

        if(auth()->attempt($r->only('email','password'))){
            $UserLogined =  auth()->user();

            return (new UserResource($UserLogined))->additional([
                "api_token" => $UserLogined->api_token
            ]);
        }

        return response()->json([
            "error"=>"Email and Password not matching"
        ]);
    }

    
}
