<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    // Users List
    public function getAllUsers(){

        $users = User::all();
        return UserResource::collection($users);

    }

    //User Logined Profile
    public function myProfile(){

        $user = auth()->user();
        return new UserResource($user);

    }

    // Remove User
    public function delete($id){
        $user = User::find($id);

        if($user){
            $user->delete();

            return response()->json([
                'msg' => "Deleted User id ".$user->id,
            ]);
        }

        return  response()->json([
            'msg' => "User Id $id Not Found",
        ]);
    }

    // Edit User
    public function edit(Request $r,$id){

        $user = User::find($id);

        $r->validate([
            'name'=>'required',
            'email'=>'required|email'
        ]);

        if($user){
            $user->update([
                'name'=> $r->name,
                'email'=> $r->email
            ]);

            return (new UserResource($user))->additional([
                'msg' => 'Updated User Id '. $user->id
            ]);
        }
        
        return response()->json([
            'msg' => 'User Id '.$id.' Not Found'
        ]);
    }

    // Detail User
    public function detail($id){
        
        $user = User::find($id);

        if($user){
            return new UserResource($user);
        }

        return response()->json([
            "msg" => "User Id ".$id." Not Found"
        ]);

    }
}
