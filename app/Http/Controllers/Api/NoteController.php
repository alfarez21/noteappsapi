<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NoteResource;
use Illuminate\Http\Request;

use App\Note;

class NoteController extends Controller
{
    
    // My Note
    public function myNotes(){

        $user = auth()->user();
        $note = Note::where('user_id',$user->id)->get();

        if($note){
            return NoteResource::collection($note);
        }

        return response()->json([
            'msg'=> 'Not Found Note'
        ]);
    }

    // Add Note
    public function addNote(Request $r){

        $r->validate([
            'note'=> 'required'
        ]);

        $user = auth()->user();

        $note = Note::create([
            'note'=>$r->note,
            'user_id'=>$user->id
        ]);

        if($note){

            return (new NoteResource($note))->additional([
                'msg'=>'add successfull'
            ]);

        }

        return response()->json([
            'msg' => 'add failed'
        ]);

    }

    // Detail Note
    public function detail($id){

        $note=Note::find($id);

        if($note){

            return new NoteResource($note);

        }

        return response()->json([
            'msg' => 'Note Id '.$id.' Not Found'
        ]);

    }

    // Delete Note
    public function delete($id){
        $note = Note::find($id);

        if($note){
            $note->delete();

            return response()->json([
                'msg' => 'Note Id '.$id.' Delete'
            ]);
        }

        return response()->json([
            'msg' => 'Note Id '.$id.' Not Found'
        ]);

    }
    
    // Edit Note
    public function edit(Request $r,$id){
        $r->validate([
            'note'=> 'required'
        ]);

        $note = Note::find($id);

        if($note){

            $note->update([
                'note'=>$r->note,
            ]);
            
            return (new NoteResource($note))->additional([
                'msg'=>'Edit successfull'
            ]);

        }

        return response()->json([
            'msg' => 'Edit failed'
        ]);
    }

}
